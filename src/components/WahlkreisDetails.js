import './css/WahlkreisDetails.css';

export default function WahlkreisDetails(props) {
    return (
        <div id="WahlkreisDetails">
        { props.wk && 
            <ul>
                <li><span className="keyname">Name:</span><span className="value">{props.wk?.name}</span></li>
                <li><span className="keyname">Titel:</span><span className="value">{props.wk?.titel}</span></li>
                <li><span className="keyname">Menge Teilnehmx:</span><span className="value">{props.wk?.teilnemx}</span></li>
                <li><span className="keyname">Alterskohorte:</span><span className="value">{props.wk?.age}</span></li>
                <li><span className="keyname">Geschlechterwahl:</span><span className="value">{props.wk?.sex}</span></li>
                { /* Should be a component. */ }
                <li><span className="keyname">Kontakte:</span><span className="value">{
                    <ul>
                    {props.wk?.kontakte?.map(k => {
                        return (
                            <div key={k.name}>
                                <li>{k.organisation}</li>
                                <li>{k.name}</li>
                                <li>{k.telefon}</li>
                                <li>{k.email}</li>
                            </div>)
                    })}
                    </ul>
                    }
                    </span></li>
            </ul>
        }
        { !props.wk &&
            <div>Kein Wahlkreis ausgewählt 	&#x1F4A9;</div>
        }

        </div>
    )
}
import { Select } from 'semantic-ui-react';

export default function WahlkreisChooser(props) {
    const selectWahlkreis = e => {
        console.log(e);
        let wkChosen = props.wahlkreise.find(wk => wk.name === e.target.textContent);
        console.log(wkChosen);
        props.setWahlkreis(wkChosen);
    }
    return (
        <Select 
            placeholder="Wahlkreis wählen"
            onChange={(e) => selectWahlkreis(e)}
            options={props.wahlkreise.map(wk => { return {key:wk.name, value:wk.name, text:wk.name}})}
        />
    )
}
import { MapContainer, FeatureGroup, TileLayer, Marker, Popup } from 'react-leaflet';
import './css/OverviewMap.css';

export default function OverviewMap(props) {
    const Leaflet = window.L;
    const bounds = Leaflet.latLngBounds(props.teilnehmx.map(t => [t.lat,t.lon]));
    console.log(bounds);
    return (
        <div id="mappingContainer">
            { /* TODO: Map center aus teilnehmx ermitteln */ }
            <MapContainer bounds={bounds}>
                <TileLayer 
                    attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <FeatureGroup>
                    { props.teilnehmx &&
                        props.teilnehmx?.map(t => {
                            return (
                                <Marker key={"" + t.lat + t.lon} position={[t.lat,t.lon]}>
                                    <Popup>
                                        Here we'd have Buttons.
                                    </Popup>
                                </Marker>
                            )
                        })
                    }
                </FeatureGroup>
            </MapContainer>
        </div>
    )
}
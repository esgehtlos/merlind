import { useEffect, useState } from 'react';
import { Tab } from 'semantic-ui-react';
import wahlkreisMock from '../mocks/wahlkreise.js';
import teilnehmxMock from '../mocks/teilnehmx.js';
import WahlkreisChooser from './WahlkreisChooser';
import WahlkreisDetails from './WahlkreisDetails';
import OverviewMap from './OverviewMap';

export default function Index() {
    const [wahlkreise, setWahlkreise] = useState([]);
    const [wahlkreis, setWahlkreis] = useState();
    const [teilnehmx, setTeilnehmx] = useState();

    useEffect(() => {
        setWahlkreise(wahlkreisMock);
        setTeilnehmx(teilnehmxMock);
    }, []);
    
    const panes = [
        {
            menuItem: 'Wahlkreis-Daten',
            render: () => <Tab.Pane attached={false}><WahlkreisDetails wk={wahlkreis}/></Tab.Pane>
        }, {
            menuItem: 'Teilnehmendenliste',
            render: () => <Tab.Pane attached={false}>(Teilnehmendenliste)</Tab.Pane>
        }, {
            menuItem: 'Teilnehmendenmap',
            render: () => <Tab.Pane attached={false}><OverviewMap teilnehmx={teilnehmx}/></Tab.Pane>
        }, {
            menuItem: 'Aktionen',
            render: () => <Tab.Pane attached={false}>(Losen)(Nachlosen)(Anschreibengenerierung)</Tab.Pane>
        }

    ]

    return (
        <div id="Index">
            <WahlkreisChooser wahlkreise={wahlkreise} setWahlkreis={setWahlkreis} />
            <div>Gewählter WK: {wahlkreis?.name ? wahlkreis.name : 'Noch keiner'}</div>
            <Tab menu={{pointing:true}} panes={panes}/>
        </div>
    )
}
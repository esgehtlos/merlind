import { Form, Button } from 'semantic-ui-react';
import { useState } from 'react';
import Index from './Index';
import './css/Login.css';

export default function Login(props) {
    const [login, setLogin] = useState();
    const [username, setUsername] = useState();
    
    const setLoginName = event => {
        event.preventDefault();
        console.log(username);
        setLogin(username)
    }

    return (
        <div className="Index">
        { !login && <div className="Login">
            <Form>
                <Form.Group widths={2} width='equal'>
                    <Form.Input label="login" placeholder='Username' onChange={(e) => setUsername(e.target.value)}/>
                    <Form.Input label='password' placeholder='Passwort'/>
                </Form.Group>
                <Button onClick={(e) => setLoginName(e)}>Log in</Button>
            </Form>
        </div>
        }
        { login && <Index/> }
        </div>
    )
}
const wahlkreise = [
    {
        name: "Coburg/Kronach",
        datum: "2020-03-21T17:59:00+01:00",
        titel: "Test",
        teilnehmx: 5,
        age: [16,18,24,36,48,60,72,200],
        sex: false,
        kontakte: [
            {
                "organisation": "Abgeordnete",
                "name": "Charlotte Müller MdB",
                "telefon": "09561/7088164",
                "email": "peter+charlotte@kornherr.net"
            }, {
                "organisation": "Meldeamt Coburg Amtsleitung",
                "name": "Lieschen Müller",
                "telefon": "09561/7088164",
                "email": "peter+lieschen@kornherr.net"
            }
        ]
    }, {
        name: "Berlin Mitte",
        datum: "2020-03-21T17:59:00+01:00",
        titel: "Test2",
        teilnehmx: 50,
        age: [16,18,24,36,48,60,72,200],
        sex: true,
        kontakte: [
            {
                "organisation": "Abgeordneter",
                "name": "Fritz Müller MdB",
                "telefon": "09561/7088164",
                "email": "peter+fritz@kornherr.net"
            }, {
                "organisation": "Polizei Berlin Meldebehörde",
                "name": "Elisabeth Müller",
                "telefon": "09561/7088164",
                "email": "peter+elisabeth@kornherr.net"
            }
        ]
    }
]

module.exports = wahlkreise;
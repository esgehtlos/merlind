const faker = require('faker/locale/de');

let teilnehmx = [];
for (let i = 0; i < 10; i++) {
    teilnehmx.push({
        vorname: faker.name.firstName(),
        nachname: faker.name.lastName(),
        dob: faker.date.between("1940-01-01T00:00:00+0100", "2010-12-31T23:59:00+0100"),
        sex: faker.name.gender(),
        strasse: faker.address.streetName(),
        hausnummer: faker.address.streetAddress(),
        plz: faker.address.zipCode(),
        ort: faker.address.city(),
        lat: faker.address.latitude(),
        lon: faker.address.longitude()
    })
}

module.exports = teilnehmx;
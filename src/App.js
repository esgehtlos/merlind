import './App.css';
import Login from './components/Login';
import 'semantic-ui-css/semantic.min.css'

function App() {
  return (
    <div className="App">
      <div className="container">
        <Login />
      </div>
    </div>
  );
}

export default App;

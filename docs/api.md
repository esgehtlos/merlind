# MERLIND API

### Dokumentation der Schnittstelle Backend/Frontend

## Middleware

* Auth
	* In jedem Request wird das JWT als HTTP-Header mitgegeben:
		* "authorization: JWT ey.….…"
	* Oder: Handling über normale Session Cookies

## Routen
### Hauptbenutzer

**Anmeldung der Admin-Usex**

_Path: /user_
<pre>
Method: post
Payload: JSON: {"username":"foo", "password":"bar"}
Return: 
    Erfolg: 200 JWT als JSON
</pre>

Hinweis: Natürlich keine Passwörter im Klartext in der Datenbank -> bcrypt

<pre>
Method get
Return: Userdaten als JSON
</pre>

### Mandanten (Wahlkreise)

**Alles rund um Wahlkreise**

_Path /wahlkreise_
<pre>
Method post
Payload: JSON:
{
    "name": "Coburg/Kronach", 
    "datum": "2020-03-03T09:00:00+0, 
    "titel": "Einwohnerräte zur Btw 2021", 
    "teilnehmx": 50, 
    "age": [16,18,24,36,48,64,200], 
    "sex": true/false, 
    "kontakte": [
        {
            "organisation": "Landratsamt Coburg", 
            "name": "Foobar, MdB", 
            "telefon": "09561/7088164", 
            "email": "foo@bar.com"
        },
        { … }
    ]
}

Return:
    Erfolg: 200 + { "wahlkreisid": … }
</pre>
* "sex" gibt an ob die paritätische Besetzung nach "Geschlecht" aktiv sein soll
* "age" gibt die Altersstufen an, die verwendet werden sollen. Das erste Element ist das Mindestalter!

<pre>
Method get
Return:
    Erfolg: 200, Wahlkreisnamen inkl. ID als JSON, {"id":110, "wahlkreis": "Coburg/Kronach"}
</pre>

_Path /wahlkreis/:id_
<pre>
Method get
Return:
    Erfolg: 200 + Wahlkreisdaten als JSON
</pre>
Die gesamten Daten, gerne auch evtl verknüpfte Entitäten (Aufsuchx)

<pre>
Method delete
Return:
    Erfolg: 200, body egal, löscht den Wahlkreis und alle seine Referenzen wie zB Teilnehmende
</pre>

<pre>
Method patch
Payload wie post
Return:
    Erfolg: 200, macht UPDATE auf den entsprechenden Wahlkreis und Ergebnis als JSON im body
</pre>

### Dateiimport

**Einfügen einer Teilnehmx-Datei**

**noch unklar: Wo findet die Normalisierung statt?**

_Path /import/:id - id ist die ID des Wahlkreises_

<pre>
Method post
Payload Teilnehmendendatei als multipart/form-data
Return:
    Erfolg: 201
</pre>

### Losfunktion

**Auslösen einer Auslosung im gegebenen Wahlkreis**

_Path /los/:id - id des Wahlkreises_

<pre>
Method get
Return:
    Erfolg: 200, body egal, Teilnehmendenliste wird gelost
</pre>

_Path /nachlosung/:id - id ist Wahlkreis_
<pre>
Method get
Return:
    Erfolg: 200, body egal, löst Neulosung aus
</pre>

### Geloste Teilnehmende abrufen

**Teilnehmx zur Anzeige**

_Path /teilnehmx/:id - id ist Wahlkreis_

<pre>
Method get
Return:
    Erfolg: 200, JSON-array mit den gelosten Teilnehmenden (inkl Geodaten)
</pre>

### Einladungen

_Path /invite/:id - id ist Wahlkreis_
<pre>
Method get
Return:
    Erfolg: 200 + Liste der Teilnehmenden, die eine Einladung bekommen sollen als JSON-array
</pre>

### PDF-Generierung

* TBD: Wo generieren wir die Einladungsbrief-PDFs und zugehörigen QRs?

### Rückmeldungen

**Gilt für 1) self-service, 2) manuellen Eingriff, 3) Aufsuchx**

_Path /rueckmeldung/:id - id des Teilnehmx_
<pre>
Method post
Payload: JSON:
{
    "teilnahme": true/false, 
    "absagegrund": "Freitext", 
    "kontakt": "telefonnummer oder email adresse", 
    "vorschlaege": "Freitext"
}
Return:
    Erfolg: 200, body egal, setzt die Teilnahme in der Teilnehmertabelle
</pre>

_Path /rueckmeldung/:token/:id - token (hash, nicht eindeutig), id des Teilnehmenden_
<pre>
Method post
Payload: wie darüber
Return:
    Erfolg: Wie darüber
</pre>

### Routen

**Alles rund um die Aufsuchx**

_Path /aufzusuchen_
<pre>
Method get
Return:
    Erfolg: 200, JSON-array der noch aufzusuchenden Teilnehmenden (inkl Geodaten)
</pre>

_Path /aufsuchx/:id - id des Wahlkreises_
<pre>
Method post
Payload: JSON { "num": 5 } # Generiert <num> Aufsuchx mit "token"
Return:
    Erfolg: 200, JSON-array der erzeugten Aufsuchx mit token
</pre>

<pre>
Method get
Return:
    Erfolg: 200, JSON-array der Aufsuchx für diesen WK mit token
</pre>

_Path /aufsuchx/:token_
<pre>
Method post
Payload JSON {"teilnehmx": id-des-Teilnehmenden}
Return:
    Erfolg: 200, body egal, ordnet das Token dex Teilnehmx zu
</pre>

<pre>
Method get
Return:
    Erfolg: JSON-array der Teilnehmx zum Token
</pre>

## Fehlerbehandlung

Je nach Fehler:

* 400: Client schickt Blödsinn
* 401: Auth geht schief/JWT oder Session nicht gültig/Zugriff auf Ressource nicht erlaubt
    * mehr Information im Body, aber **keine sensiblen Daten!**
* 404: Ressource nicht existent
* 500: Backend macht Blödsinn
* 666: Die Welt ist endgültig am Arsch

## Ergänzungen
Sind 1) Willkommen und 2) bitte über Stefan und/oder Peter routen. Send patches!
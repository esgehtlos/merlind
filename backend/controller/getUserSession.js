/**
 *
 * @param {Request} req
 * @param res
 */
module.exports = (req, res) => {
    res.json({user: req.user})
}
const jwt = require('jsonwebtoken');
const {config} = require('./../lib/config');

/**
 *
 * @param {Request} req
 * @param res
 */
module.exports = async (req, res) => {
    const user = req.user;
    const token = await jwt.sign({ user }, config.get('JWT_SECRET'), { algorithm: 'HS256', expiresIn: config.get('JWT_EXPIRES_IN')});
    res.json({token})
}
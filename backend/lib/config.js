const nconf = require('nconf');

const defaults = {
        API_HTTP_PORT: 1337,
        BCRYPT_SALT_ROUNDS: 10,
        // TODO: Should be otbained in runtime via secret management
        JWT_SECRET: "pssssssst",
        JWT_EXPIRES_IN: "1h",
};

nconf.use('memory');
nconf.env();
nconf.defaults(defaults);

if (
    nconf.get('API_HTTP_PORT') &&
    typeof nconf.get('API_HTTP_PORT') === 'string'
) {
        nconf.set(
            'API_HTTP_PORT',
            parseInt(nconf.get('API_HTTP_PORT'), 10),
        );
}

module.exports = {
        defaults,
        config: nconf
}

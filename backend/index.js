const express = require('express')
const auth = require('basic-auth')
const jwt = require('jsonwebtoken')
// TODO: Use HTTPS!
const http = require('http')
const fs = require('fs')
const yaml = require('yaml')
const User = require('./models/User')

const initializeSwagger = require('swagger-tools').initializeMiddleware;
const {config} = require('./lib/config');
const controllers = require('./controller');
const app = express()
const port = config.get('API_HTTP_PORT');

const file = fs.readFileSync('./docs/api.yaml', 'utf8')
const spec = yaml.parse(file);

// Initialize the Swagger middleware
initializeSwagger(spec, middleware => {
    // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
    app.use(middleware.swaggerMetadata());

    // Validate Swagger requests
    app.use(middleware.swaggerValidator());

    app.use(middleware.swaggerSecurity({
        basicAuth: async (req, authOrSecDef, scopesOrApiKey, callback) => {
            try {
                const {name: username, pass: password} = auth(req);
                const user = await User.findOne(username);

                if (!user) {
                    return callback(new Error())
                }

                const isPasswordValid = await User.verifyPassword(password, user.password);

                if (!isPasswordValid) {
                    return callback(new Error())
                }

                delete user.password;

                req.user = user;

                return callback();
            } catch (e) {
                return callback(e)
            }
        },
        apiKey: async (req, authOrSecDef, scopesOrApiKey, callback) => {
            try {
                const apiKey = req.header('x-api-key');

                await jwt.verify(apiKey, config.get('JWT_SECRET'));

                const token = jwt.decode(apiKey, config.get('JWT_SECRET'));

                const user = token.user;

                if (!user) {
                    return callback(new Error())
                }
                req.user = user;
                return callback();
            } catch (e) {
                return callback(e)
            }
        }
    }))

    // Route validated requests to appropriate controller
    app.use(middleware.swaggerRouter({
        controllers
    }));

    // Serve the Swagger documents and Swagger UI
    app.use(middleware.swaggerUi({
        apiDocs: '/api/v1/docs/spec',
        swaggerUi: '/api/v1/docs'
    }));

    app.use((err, req, res, next) => {
        console.error(err);
        res.json({error: err});
    })

    // Start the server
    http.createServer(app).listen(port, () => {
        console.log(`Your server is listening on port ${port}`);
    });
});
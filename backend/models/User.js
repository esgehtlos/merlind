const bcrypt = require('bcrypt');
const fs = require('fs');
const yaml = require('yaml');
const {promisify} = require('util');

const comparePasswords = promisify(bcrypt.compare);
const readFile = promisify(fs.readFile);

class User {
    /**
     * Obtains and returns a user by username
     * @param username
     * @return {*}
     */
    async findOne(username) {
        // TODO: Read from a database instead of a file
        const buffer = await readFile('./backend/data/users.yaml')
        const users = yaml.parse(buffer.toString('utf-8'));
        return users.users.find(user => user.username === username);
    }

    /**
     * Verifies a given password against a given bcrypt hash of the password
     * @param password
     * @param hash
     */
    async verifyPassword(password, hash) {
        return comparePasswords(password, hash)
    }
}

module.exports = new User();